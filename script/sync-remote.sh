#!/bin/bash

function run_rsync {
    rsync -rzP \
        --exclude ".*/" \
        --exclude ".*" \
        --exclude "data/" \
        --exclude "node_modules/" \
        --exclude "evaluation/" \
        $(pwd) da@htr01:/home/da/package-hunter2/
}

run_rsync; fswatch -or --exclude ./node_modules --exclude ./.git --exclude ./evaluation . | while read f; do run_rsync; done
