'use strict' /* eslint-env mocha */

const User = require('../src/user.js')
const assert = require('assert')

describe('User Test', function () {
  describe('getAllUsers', function () {
    beforeEach(function () {
      this.alice = { name: 'alice', hash: 'abcd' }
      this.bob = { name: 'bob', hash: '1234' }
      this.store = [this.alice, this.bob]
    })

    it('returns an array', function () {
      const users = User.getAllUsers(this.store)
      assert(Array.isArray(users))
    })

    it('returns all users from the store', function () {
      const users = User.getAllUsers(this.store)
      assert.strictEqual(users.length, 2)
      assert(users.some(x => x.name === this.alice.name && x.hash === this.alice.hash))
      assert(users.some(x => x.name === this.bob.name && x.hash === this.bob.hash))
    })
  })
})
