'use strict' /* eslint-env mocha */

const basicAuth = require('express-basic-auth')

const app = require('express')()
const route = require('../src/route')
const sinon = require('sinon')
const assert = require('assert')

describe('Route Test', function () {
  describe('Takes NODE_ENV into account when requiring authentication', function () {
    before(function () {
      this.originalNodeEnv = process.env.NODE_ENV
    })

    beforeEach(function () {
      this.opts = {
        jobStore: {},
        pendingContainer: {},
        workdir: '/tmp/package-hunter-workdir'
      }

      this.spy = sinon.spy(app, 'post')
    })

    afterEach(function () {
      delete process.env.NODE_ENV
      this.spy.restore()
    })

    after(function () {
      if (this.originalNodeEnv) { process.env.NODE_ENV = this.originalNodeEnv }
    })

    it('requires no auth if NODE_ENV is development', async function () {
      process.env.NODE_ENV = 'development'

      route(app, this.opts)
      const expectedAuthFn = basicAuth({ users: {} }).toString()
      assert(!this.spy.args.some(x => x[0] === '/monitor/*' && x[1].toString() === expectedAuthFn),
        'Expected authorization middleware configured for routes /monitor/*')
    })

    it('requires auth if NODE_ENV is production', async function () {
      process.env.NODE_ENV = 'production'

      route(app, this.opts)
      const expectedAuthFn = basicAuth({ users: {} }).toString()
      assert(this.spy.args.some(x => x[0] === '/monitor/*' && x[1].toString() === expectedAuthFn),
        'Expected authorization middleware configured for routes /monitor/*')
    })
  })
})
