# Package Hunter

A tool for identifying malicious dependencies via runtime monitoring.

**We run Package Hunter interally at GitLab since November 2020, but it's still early days for the project.
Please [report](https://gitlab.com/gitlab-org/security-products/package-hunter/-/issues) any bugs you might find.**

## Learn more

Learn more in the [GitLab blog post introducing Package Hunter](https://about.gitlab.com/blog/2021/07/23/announcing-package-hunter/) and the [Falco blog post diving into the architecture and getting started](https://falco.org/blog/gitlab-falco-package-hunter/).

Follow the `#EveryoneCanContribute cafe` meetups with an introduction to Falco, Package Hunter and live demos as recordings:

- [42. #EveryoneCanContribute cafe: Falco and GitLab Package Hunter](https://everyonecancontribute.com/post/2021-08-11-cafe-42-falco-gitlab-package-hunter/) introduces Falco and tried the Vagrant setup of Package Hunter. This did not work initially, the group decided to move into a cloud setup for the next cafe meetup.
- [43. #EveryoneCanContribute cafe: More Package Dependency Hunting with GitLab](https://everyonecancontribute.com/post/2021-08-18-cafe-43-more-package-dependency-hunting-with-gitlab/) provides a [slide deck introducing Package Hunter](https://docs.google.com/presentation/d/1biVRpHGBeHJvUeeySOrPAW8qFmls-EvWEj9_JVHVnuw/edit), a [Terraform module for Hetzner Cloud](https://gitlab.com/everyonecancontribute/security/terraform-hcloud-packagehunter-host) and a live hacking session of all Package Hunter features together with Dennis Appelt. 

## Installation

Prerequisites:
- Falco [0.29.0](https://download.falco.org/packages/bin/x86_64/falco-0.29.1-x86_64.tar.gz) (newer versions might be incompatible).
  See the Falco project for [install instructions](https://falco.org/docs/getting-started/installation/). 
- Docker 20.10 or newer
- Node v12.21 or newer

Falco's gRPC API must be enabled. Follow the [setup instructions](https://falco.org/docs/grpc/) to enable the gRPC API and make sure 
`server.crt`, `client.crt`, and `client.key` are located at `/etc/falco/certs`.

Execute below commands in a terminal to install Package Hunter:
```sh
git clone https://gitlab.com/gitlab-org/security-products/package-hunter.git
cd package-hunter
cp falco/falco_rules.local.yaml /etc/falco/ && service falco restart
npm ci

# create a user for authenticating calls to the Package Hunter API
scripts/create-user

DEBUG=pkgs* node src/server.js
```

This will start a server process listening on port 3000.

For running Package Hunter on your local computer see [Development](#development).

## Usage

See https://gitlab.com/gitlab-org/security-products/package-hunter-cli for how to analyze your program's dependencies with Package Hunter.

## Development

### Installation

A Vagrantfile is provided to easily provision a virtual machine to run Package Hunter locally:

1. [Download and install](https://www.vagrantup.com/downloads) Vagrant for your operating system.
2. Open a terminal and navigate to the Package Hunter project directory.
3. Run the command `vagrant up` and wait for the initialization to finish.
4. You now have a virtual machine ready to run Package Hunter. Execute the command `vagrant ssh` to obtain a command shell on the guest virtual machine.
5. Inside the guest machine, execute `cd /vagrant` to go to the project's files (these files are automatically synced between the host and guest).
6. Execute command `npm ci`

If you want to run Package Hunter directly in Linux, please refer to the `config.vm.provision "shell"` block of the `Vagrantfile` for the necessary commands to set up Package Hunter.

### Usage

1. On the host machine, execute `vagrant ssh` if you don't already have an SSH session on the guest machine.
2. Execute `NODE_ENV=development DEBUG=pkgs* node src/server.js` to run the Package Hunter web server on port 3000.

Port `3000/tcp` (Package Hunter web server) and `5060/tcp` (gprc) are forwarded and available to the host machine, so the following commands will work from the host:

### Submit a new project to be analysed by your local Package Hunter instance:

```sh
curl -v -H 'Content-Type: application/octet-stream' --data-binary @gitlab-master.tar.gz http://localhost:3000/monitor/project/yarn
```

If there is no error, the response will be a JSON object like `{"status":"ok","id":"c93c35ef-4de2-43a6-9885-56d3a3d97420"}`. Package Hunter analyses your project asynchronously. You can query the status and result of a job with `id`, e.g. `curl -v http://localhost:3000/?id=93c35ef-4de2-43a6-9885-56d3a3d97420`

The example curl request is using the endpoint `/monitor/project/yarn`, which can be used to analyze projects that use yarn. The endpoints `/monitor/project/npm` or `/monitor/project/bundler` can be used instead for projects using npm or bundler, respectively.

## Caveats

- If your program requires build dependencies that are not installed in the [default base image](./falco-test/Dockerfile),
you can provide an alternative base image with the required build dependencies. To do so, switch to the Package Hunter
installation folder and run:

```sh
cd falco-test

# Set BASE_IMAGE and BASE_TAG to an image that contains the build dependencies
# required to run "[npm|yarn|bundle] install" for your application
# (e.g. C++ compiler toolchain).
# For example, for a basic NodeJS app the values should be set to:
docker build --build-arg BASE_IMAGE=node --build-arg BASE_TAG=latest -t maldep .
```

- Package Hunter executes dependencies within the docker container under the user `root`. Consider configuring
  your docker instllation to use [user namespaces](https://docs.docker.com/engine/security/userns-remap/)
  to prevent privilege-escalation attacks from within a container.

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).
