const Address6 = require('ip-address').Address6

const IPv6EgressConnectionRegex = /^::ffff:172\.17\.0\.2:\d+->([a-f0-9:]+):443$/

class AlertFilter {
  constructor (alert) {
    this.alert = alert
  }

  isDiscardable () {
    return this.isBundlerIPv6()
  }

  isBundlerIPv6 () {
    // Check for required output fields
    if (!this.alert.output_fields['fd.name'] || !this.alert.output_fields['proc.cmdline']) {
      return false
    }

    // is it Bundler installation?
    if (!this.alert.output_fields['proc.cmdline'].startsWith('bundle /usr/local/bin/bundle install')) {
      return false
    }

    // is it an egress IPv6 connection?
    const match = this.alert.output_fields['fd.name'].match(IPv6EgressConnectionRegex)
    if (!match) {
      return false
    }

    // Is the destination address correct?
    const destAddr = new Address6(match[1])
    return (destAddr.isCorrect() && destAddr.address.endsWith(':ffff'))
  }
}

module.exports = AlertFilter
